import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:login_bloc/src/blocs/validators.dart';

class Bloc with Validators {
  final _email = BehaviorSubject<String>(); //Alternativa de rx a StreamController<String>().broadcast();
  final _password = BehaviorSubject<String>();  //Alternativa de rx StreamController<String>().broadcast();

  //Add data to stream
  Function(String) get changeEmail => _email.sink.add;
  Function(String) get changePassword => _password.sink.add;

  //Retrieve data from stream
  Stream<String> get email => _email.stream.transform(validateEmail);
  Stream<String> get password =>
      _password.stream.transform(validatePassword);
  Stream<bool> get submitValid => Observable.combineLatest2(email, password, (e,p) => true);

  submit(){    
    final validEmail = _email.value; //Aquì estará el último valor correcto de _email
    final validPassword = _password.value;

    print('Email is $validEmail');
    print('Email is $validPassword');    
  }

  void dispose() {
    _email.close();
    _password.close();
  }
}

final bloc = Bloc();
